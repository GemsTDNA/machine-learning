import numpy as np
import pandas as pd
from sklearn import svm
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split

coloumn_no_1 = 2
coloumn_no_2 = 3

df = pd.read_excel('str_decomposition.xlsx')
#df = pd.read_excel('luminol_data.xlsx')

series_1 = []
series_2 = []
for i in range(len(df)):
    series_1.append(df.iloc[i,coloumn_no_1])   
    series_2.append(df.iloc[i,coloumn_no_2])

metrics_data = np.asarray(map(lambda (x,y): [x, y], zip(series_1, series_2)))

X_train, X_test = train_test_split(metrics_data, test_size=0.10)

X_train = np.asarray(map(lambda (x,y): [x, y], zip(series_1, series_2)))

print len(X_train)
print len(X_test)

clf = svm.OneClassSVM(nu=0.1, kernel="rbf", gamma=0.1)

clf.fit(X_train)

#X_test = np.array([[3,4000],[4,5000],[5,4150],[3,3000],[4,2500],[6,2500]])
predict_value = clf.predict(X_test)
#print "Anomaly value: ", (predict_value)

in_index_val = []
out_index_val = []
for i in range(len(predict_value)):
    if (predict_value[i] == 1.0):
        in_index_val.append(i)
    else:
        out_index_val.append(i)

plt.scatter(X_train[:, 0], X_train[:, 1], c='blue')
#plt.scatter(X_test[:, 0], X_test[:, 1], c='green')
for val in in_index_val:
    plt.scatter(X_test[val][0], X_test[val][1], c='orange', marker = 'X')
for val in out_index_val:
    plt.scatter(X_test[val][0], X_test[val][1], c='red', marker = 'x')
plt.show()