# -*- coding: utf-8 -*-
"""
Created on Fri Sep 22 10:14:09 2017

@author: lingaselvan
"""

import numpy as np
import pandas as pd
from sklearn.svm import SVC
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split

coloumn_no_1 = 0
coloumn_no_2 = 1
coloumn_no_3 = 2

df = pd.read_excel('C:\Users\lingaselvan\Documents\Python Scripts\Data\svm_three_data.xlsx')

series_1 = []
series_2 = []
series_3 = []
for i in range(len(df)):
    series_1.append(df.iloc[i,coloumn_no_1])   
    series_2.append(df.iloc[i,coloumn_no_2])
    series_3.append(df.iloc[i,coloumn_no_3])   

print series_1[0]
print series_2[0]
print series_3[0]

metrics_data = np.asarray(map(lambda (x, y, z): [x, y, z], zip(series_1, series_2, series_3)))

target_value = []
for i in range(len(df)):
    if ((series_1[i] < 3000) and (series_2[i] < 4) and (series_3[i] < 4000)):
        target_value.append(1)
    else:
        target_value.append(0)

#print target_value

pca = PCA(n_components=2)
reduced_data_pca = pca.fit_transform(metrics_data)
print reduced_data_pca

X, y = reduced_data_pca, np.asarray(target_value)
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)

scaler = StandardScaler().fit(X_train)
X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test)

svc = SVC(kernel='linear')
svc.fit(X_train, y_train)

test_data =  np.asarray([[1, 2, 3],[1, 3, 700],[5, 4, 10]])
y_pred = svc.predict(scaler.transform(pca.fit_transform(test_data)))

print y_pred