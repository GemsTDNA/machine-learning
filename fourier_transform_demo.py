import numpy as np
from scipy.fftpack import fft
import matplotlib.pyplot as plt
from scipy.signal import blackman
import random

# Signal Parameters
number_of_samples  = 1000
frequency_of_signal  = 5
sample_time = 0.001
amplitude = 1

signal = [amplitude * np.sin((2 * np.pi) * frequency_of_signal * ii * sample_time) for ii in range(number_of_samples)]
s_time = [ii * sample_time for ii in range(2*number_of_samples)]

frequency_of_signal  = 10
sample_time = 0.001
amplitude = 1
test = [amplitude * np.sin((2 * np.pi) * frequency_of_signal * ii * sample_time) for ii in range(number_of_samples)]
signal = signal + test

plt.figure(figsize=(12, 6));
plt.plot(s_time, signal);
plt.show()

yf = fft(signal)
xf = np.linspace(0.0, 1.0/(2.0*0.001), 2000//2)
plt.figure(figsize=(12, 6));
plt.plot(xf, 2.0/2000 * np.abs(yf[0:2000//2]))
plt.grid()
plt.show()

print 1/(xf[abs(yf).tolist().index(sorted(abs(yf))[-5])])