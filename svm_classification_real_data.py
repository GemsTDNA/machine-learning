from sklearn import datasets, preprocessing
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.decomposition import PCA
from sklearn import svm

iris = datasets.load_iris()

print type(iris.data)

pca = PCA(n_components=2)
reduced_data_pca = pca.fit_transform(iris.data)

X, y = reduced_data_pca, iris.target

X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=33)

scaler = preprocessing.StandardScaler().fit(X_train)
X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test)

svc_model = svm.SVC(gamma=0.001, C=100., kernel='linear')

svc_model.fit(X_train, y_train)

y_pred = svc_model.predict(X_test)

print svc_model.score(X_test, y_test)
print accuracy_score(y_test, y_pred)
