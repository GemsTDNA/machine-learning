
# coding: utf-8

# In[19]:

from datetime import datetime
import pandas as pd
import time
import matplotlib.pyplot as plt
import matplotlib.dates as md
import dateutil

coloumn_no = 2
df = pd.read_excel('luminol_data.xlsx')
#df = pd.read_excel('str_decomposition.xlsx')
series = []
datestrings = []
for i in range(len(df)):
    series.append(df.iloc[i,coloumn_no])
    datestrings.append(df.iloc[i,0])
    
dates = [dateutil.parser.parse(s) for s in datestrings]
plt.subplots_adjust(bottom=0.2)
plt.xticks( rotation=25 )
ax=plt.gca()
ax.set_xticks(dates)
xfmt = md.DateFormatter('%Y-%m-%d %H:%M:%S')
ax.xaxis.set_major_formatter(xfmt)
plt.plot(dates,series, "o-")
plt.show()
