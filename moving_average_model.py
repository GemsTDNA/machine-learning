
# coding: utf-8

# In[29]:

import numpy as np
import pandas as pd
from numpy import mean
from sklearn.metrics import mean_squared_error
from matplotlib import pyplot

coloumn_no = 1
df = pd.read_excel('C:\Users\lingaselvan\Documents\Python Scripts\Data\shampoo_data.xlsx')
series = []
for i in range(len(df)):
    series.append(df.iloc[i,coloumn_no])
print "First value in series : ", series[0], series[1]
print "Last value in series : ", series[len(df)-1]
print "Length of series: ", len(series)

# prepare situation
X = np.asarray(series)
window = 6

history = [X[i] for i in range(window)]
test = [X[i] for i in range(window, len(X))]
predictions = list()

# walk forward over time steps in test
for t in range(len(test)):
    length = len(history)
    yhat = mean([history[i] for i in range(length-window,length)])
    obs = test[t]
    predictions.append(yhat)
    history.append(obs)
    print('predicted=%f, expected=%f' % (yhat, obs))
    print round((abs(yhat - obs)*100)/(abs(yhat + obs)/2))
error = mean_squared_error(test, predictions)
print('Test MSE: %.3f' % error)

# plot
pyplot.plot(test)
pyplot.plot(predictions, color='red')
pyplot.show()

# zoom plot
#pyplot.plot(test[0:100])
#pyplot.plot(predictions[0:100], color='red')
#pyplot.show()


# In[31]:




# In[ ]:



