# -*- coding: utf-8 -*-
"""
Created on Thu Sep 28 12:36:28 2017

@author: lingaselvan
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

coloumn_no = 0
df = pd.read_excel('C:\Users\lingaselvan\Documents\Python Scripts\Data\data.xlsx')
series = []
for i in range(len(df)):
    series.append(df.iloc[i,coloumn_no])

print "First value: ", series[0]


arr = series
plt.plot(arr)

plt.ylabel('some numbers')

plt.show()

elements = np.array(arr)

mean = np.mean(elements, axis=0)

sd = np.std(elements, axis=0)

final_list = [x for x in arr if (x > mean - 2 * sd)]

final_list = [x for x in final_list if (x < mean + 2 * sd)]

#print(final_list)

plt.plot(final_list)

plt.ylabel('some numbers')

plt.show()