# coding: utf-8
import numpy as np
import pandas as pd
from scipy.fftpack import fft
import matplotlib.pyplot as plt

coloumn_no = 1
df = pd.read_excel('C:\Users\lingaselvan\Documents\Python Scripts\Data\cpu_data_08_09.xlsx')

series = []
for i in range(len(df)):
    series.append(df.iloc[i,coloumn_no])
print "First value in series : ", series[0]
print "Last value in series : ", series[len(df)-1]
print "Length of series: ", len(series)

# Number of sample points
N = len(series)
Fs = 60
# sample spacing
T = 1.0/Fs
x = np.linspace(0.0, N*T, N)
y = np.asarray(series)
plt.figure(figsize=(12, 6));
plt.plot(x, y)
plt.grid()
plt.show()

yf = fft(y)
xf = np.linspace(0.0, 1.0/(2.0*T), N//2)
plt.figure(figsize=(12, 6));
#plt.plot(xf, 2.0/N * np.abs(yf[0:N//2]))
plt.plot(xf[1:], 2.0/N * np.abs(yf[0:N/2])[1:])
plt.grid()
plt.show()

#print xf[abs(yf).tolist().index(sorted(abs(yf))[-2])]
print "Period: ", 1/(xf[abs(yf).tolist().index(sorted(abs(yf))[-2])])