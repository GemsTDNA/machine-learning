import numpy as np
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

coloumn_no_1 = 2
coloumn_no_2 = 13
coloumn_no_3 = 21

df = pd.read_excel('str_decomposition.xlsx')

series_1 = []
series_2 = []
series_3 = []
for i in range(len(df)):
    series_1.append(df.iloc[i,coloumn_no_1])   
    series_2.append(df.iloc[i,coloumn_no_2])
    series_3.append(df.iloc[i,coloumn_no_3])

print series_1[0]
print series_2[0]
print series_3[0]

X_train = np.asarray(map(lambda (x,y,z): [x, y, z], zip(series_1, series_2, series_3)))
X_test = np.array([[3.286366148, 1891.311265, 1460.352895],[5, 2000, 1500],[10, 5000, 3000]])
anomalymodel = lsanomaly.LSAnomaly()
anomalymodel.fit(X_train)
anomalymodel.predict(X_test)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(X_train[:, 0], X_train[:, 1], X_train[:, 2], c='r', marker='o')
ax.scatter(X_test[:, 0], X_test[:, 1], X_test[:, 2], c='b', marker='o')
ax.set_xlabel('X Label')
ax.set_ylabel('Y Label')
ax.set_zlabel('Z Label')
plt.show()