# -*- coding: utf-8 -*-
"""
Created on Fri Sep 29 12:21:08 2017

@author: lingaselvan
"""
from random import randint
import numpy as np
import random

start_range = 1
stop_range = 100

class Particle(object):
	def __init__(self):
		self.currentParticle = np.array([randint(start_range,stop_range), randint(start_range,stop_range), randint(start_range,stop_range)])
		self.currentParticleFitness = fitness_function(self.currentParticle)
		self.pBestParticle = self.currentParticle
		self.pBestParticleFitness = fitness_function(self.pBestParticle)
		self.pVelocity = 0
		self.pPosition = 0
		self.c1 = 2
		self.c2 = 2


	def update_particle(self, gBest):
		self.pVelocity = self.pVelocity + self.c1*random.random()*(self.pBestParticle-self.currentParticle) + self.c2 * random.random()*(gBest-self.currentParticle)
#		print "Velocity value        >>>>>", self.pVelocity 
		self.pPosition = self.pPosition + self.pVelocity 
		if fitness_function(self.pPosition) < fitness_function(self.pBestParticle):
			self.pBestParticle = self.pPosition

class Swarm(object):
	def __init__(self, p=500):
		self.particles = []
		for i in range(p):
			self.particles.append(Particle())
#			print "Initial position        >>>>>", self.particles[i].currentParticle
#			print "Best known position     >>>>>", self.particles[i].pBestParticle
#			print "Initial position Fit    >>>>>", self.particles[i].currentParticleFitness
#			print "Best known position Fit >>>>>", self.particles[i].pBestParticleFitness
#			print "\n"
		self.gBestParticle = np.array([randint(start_range,stop_range), randint(start_range,stop_range), randint(start_range,stop_range)])
		self.gBestParticleFitness = fitness_function(self.gBestParticle)

	def gBestCalc(self, p=500):
		for i in range (len(self.particles)):
			#print "1 >>>>>", self.particles[i].pBestParticleFitness
			#print "2 >>>>>", self.gBestParticleFitness
			self.particles[i].update_particle(self.gBestParticle)
			if self.particles[i].pBestParticleFitness < self.gBestParticleFitness:
				self.gBestParticle = self.particles[i].pBestParticle
				self.gBestParticleFitness = self.particles[i].pBestParticleFitness
		
#		print "Global best position >>>>>", self.gBestParticle		
#		print "Global best fitness  >>>>>", self.gBestParticleFitness
				
	
def fitness_function(particles):
	productValue = particles[0]+particles[1]+particles[2]
	fitnessValue = abs(productValue-50)
	return fitnessValue

swarm = Swarm()

swarm.gBestCalc(500)

print "Result: ", swarm.gBestParticle
print "Sum: ", sum(swarm.gBestParticle)
#from operator import mul
#print "Product: ", reduce(mul, swarm.gBestParticle, 1)