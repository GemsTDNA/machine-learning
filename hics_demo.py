# -*- coding: utf-8 -*-

from scipy.io import arff
import pandas as pd
from scipy import stats
import itertools
import random
import numpy as np
from sklearn.neighbors import NearestNeighbors
from sklearn.preprocessing import MinMaxScaler
from sklearn import metrics

data, meta = arff.loadarff('C:\Users\lingaselvan\Documents\Python Scripts\Data\\ann_thyroid.arff')
df = pd.DataFrame(data)

var_list = list(df.columns.values)
del var_list[-1]
combo_list = list(itertools.permutations(var_list, 2))

for val in combo_list:
	print "Metrics: %s & %s" %(val[0], val[1])
	var_index = df[val[0]].rank()/max(df[val[0]].rank())
	k = stats.ks_2samp(df[val[1]].values, df[(var_index>0.65)][val[1]].values)
	print "k-test"
	print k.pvalue
	if (k.pvalue > 0.01):
		print "Low contrast subspace"
	else:
		print "High contrast subspace"
	print "\n"
	
	

# var0002_index = df['var_0002'].rank()/max(df['var_0002'].rank())

# t=stats.ttest_ind(df['var_0003'].values, df[(var0002_index>0.65)]['var_0003'].values, equal_var = False)
# print "t-test"
# print t.pvalue

# k = stats.ks_2samp(df['var_0003'].values, df[(var0002_index>0.65)]['var_0003'].values)
# print "k-test"
# print k.pvalue

# var0003_index = df['var_0003'].rank()/max(df['var_0003'].rank())

# t=stats.ttest_ind(df['var_0000'].values, df[(var0003_index>0.65)]['var_0000'].values, equal_var = False)
# print "t-test"
# print t.pvalue

# k=stats.ks_2samp(df['var_0000'].values, df[(var0003_index>0.65)]['var_0000'].values)
# print "k-test"
# print k.pvalue