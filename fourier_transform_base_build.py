
# coding: utf-8

# In[1]:

import numpy as np
import pandas as pd
from scipy.fftpack import fft
import matplotlib.pyplot as plt
from scipy.signal import blackman


# In[2]:

coloumn_no = 2
df = pd.read_excel('C:\Users\lingaselvan\Documents\Python Scripts\Data\str_decomposition.xlsx')
series = []
for i in range(len(df)):
    series.append(df.iloc[i,coloumn_no])
print "Series : ", series[0]


# In[3]:

# Number of sample points
N = len(series)
# sample spacing
T = 1.0
x = np.linspace(0.0, N*T, N)
y = np.asarray(series)
plt.plot(x, y)
plt.grid()
plt.show()

print len(x)
print len(y)


# In[4]:

yf = fft(y)
xf = np.linspace(0.0, 1.0/(2.0*T), N//2)
plt.plot(xf, 2.0/N * np.abs(yf[0:N//2]))
plt.grid()
plt.show()

print len(xf)
print len(2.0/N * np.abs(yf[0:N//2]))


# In[5]:

#max_y = max(yf)  # Find the maximum y value
#max_x = xf[yf.argmax()]  # Find the x value corresponding to the maximum y value
#print max_x, max_y
#print xf[abs(yf).tolist().index(sorted(abs(yf))[-4])]
print 1/(xf[abs(yf).tolist().index(sorted(abs(yf))[-2])])


# In[6]:

w = blackman(N)
ywf = fft(y*w)
plt.semilogy(xf[1:N//2], 2.0/N * np.abs(yf[1:N//2]), '-b')
plt.semilogy(xf[1:N//2], 2.0/N * np.abs(ywf[1:N//2]), '-r')
plt.legend(['FFT', 'FFT w. window'])
plt.grid()
plt.show()


# In[7]:

print 1/(xf[abs(ywf).tolist().index(sorted(abs(ywf))[-2])])


# In[ ]:




# In[ ]:



