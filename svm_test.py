# -*- coding: utf-8 -*-
"""
Created on Fri Sep 22 10:05:44 2017

@author: lingaselvan
"""

import numpy as np
import pandas as pd
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split

coloumn_no_1 = 0
coloumn_no_2 = 1

df = pd.read_excel('C:\Users\lingaselvan\Documents\Python Scripts\Data\svm_classification.xlsx')

series_1 = []
series_2 = []

for i in range(len(df)):
    series_1.append(df.iloc[i,coloumn_no_1])   
    series_2.append(df.iloc[i,coloumn_no_2])

print series_1[0]
print series_2[0]

metrics_data = np.asarray(map(lambda (x, y): [x, y], zip(series_1, series_2)))

target_value = []
for i in range(len(df)):
    if ((series_1[i] < 4) and (series_2[i] < 3000)):
        target_value.append(1)
    else:
        target_value.append(0)

#print target_value

X, y = metrics_data, np.asarray(target_value)
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=33)

scaler = StandardScaler().fit(X_train)
X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test)

svc = SVC(kernel='linear')
svc.fit(X_train, y_train)

y_pred = svc.predict(scaler.transform(np.asarray([[1, 2000],[4, 3000],[5, 2500]])))

print y_pred