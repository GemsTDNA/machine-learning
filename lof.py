#print(__doc__)

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.neighbors import LocalOutlierFactor

coloumn_no_1 = 0
coloumn_no_2 = 1

df = pd.read_excel('C:\Users\lingaselvan\Documents\Python Scripts\Data\lof.xlsx')

series_1 = []
series_2 = []

for i in range(len(df)):
    series_1.append(df.iloc[i,coloumn_no_1])   
    series_2.append(df.iloc[i,coloumn_no_2])

X = np.asarray(map(lambda (x, y): [x, y], zip(series_1, series_2)))

# fit the model
clf = LocalOutlierFactor(n_neighbors=20)
y_pred = clf.fit_predict(X)
y_pred_outliers = y_pred[10:]

print y_pred
ind = []
count = 0
for item in y_pred:
	if (item == -1):
		ind.append(count)
		count += 1
	else:
		count += 1
print ind

outli = []
for i in ind:
	print X[i]
	outli.append(X[i])

outli_arr = np.asarray(outli)
		
	
# plot the level sets of the decision function
xx, yy = np.meshgrid(np.linspace(-5, 5, 50), np.linspace(-5, 5, 50))
Z = clf._decision_function(np.c_[xx.ravel(), yy.ravel()])
Z = Z.reshape(xx.shape)

plt.title("Local Outlier Factor (LOF)")
plt.contourf(xx, yy, Z, cmap=plt.cm.Blues_r)

a = plt.scatter(X[:10, 0], X[:10, 1], c='white',
                edgecolor='k', s=20)
b = plt.scatter(outli_arr[0], outli_arr[1], c='red',
                edgecolor='k', s=20)
plt.axis('tight')
plt.xlim((-5, 5))
plt.ylim((-5, 5))
plt.legend([a, b],
           ["normal observations",
            "abnormal observations"],
           loc="upper left")
plt.show()