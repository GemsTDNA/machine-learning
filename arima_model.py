
# coding: utf-8

# In[11]:

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from statsmodels.tsa.arima_model import ARIMA
from sklearn.metrics import mean_squared_error
from pandas.plotting import autocorrelation_plot


# In[12]:

coloumn_no = 1
df = pd.read_excel('C:\Users\lingaselvan\Documents\Python Scripts\Data\cpu_data_31_08.xlsx')
series = []
for i in range(len(df)):
    series.append(df.iloc[i,coloumn_no])
print "First value in series : ", series[0]
print "Last value in series : ", series[len(df)-1]
print "Length of series: ", len(series)


# In[13]:

plt.figure(figsize=(12, 6));
autocorrelation_plot(series)
plt.show()


# In[14]:

X = series
size = int(len(X) * 0.66)
train, test = X[0:size], X[size:len(X)]
history = [x for x in train]
predictions = list()
for t in range(len(test)):
    model = ARIMA(history, order=(5,1,0))
    model_fit = model.fit(disp=0)
    output = model_fit.forecast()
    yhat = output[0]
    predictions.append(yhat)
    obs = test[t]
    history.append(obs)
    print('predicted=%f, expected=%f' % (yhat, obs))
error = mean_squared_error(test, predictions)
print('Test MSE: %.3f' % error)
# plot
plt.figure(figsize=(12, 6));
plt.plot(test)
plt.plot(predictions, color='red')
plt.show()


# In[ ]:




# In[ ]:



