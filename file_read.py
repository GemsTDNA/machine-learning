# -*- coding: utf-8 -*-
"""
Created on Wed Sep 27 17:40:42 2017

@author: lingaselvan
"""

lookup = 'GEMS-Rule-Name:SMSCCDR'

filename = 'C:\Users\lingaselvan\Documents\Python Scripts\Data\demo.txt'

line_num = []
with open(filename) as myFile:
    for num, line in enumerate(myFile, 1):
        if lookup in line:
            print 'found at line:', num
            line_num.append(num)
val_1 = line_num[0]
val_2 = line_num[1]

fin = open( filename, "r" )
data_list = fin.readlines()
fin.close()

del data_list[(val_1-1):val_2]

fout = open(filename, "w")
fout.writelines(data_list)
fout.close()