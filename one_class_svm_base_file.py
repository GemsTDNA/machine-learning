
# coding: utf-8

# In[64]:

import numpy as np
import pandas as pd
from sklearn import svm
import matplotlib.pyplot as plt

coloumn_no_1 = 2
coloumn_no_2 = 3

df = pd.read_excel('str_decomposition.xlsx')

series_1 = []
series_2 = []
for i in range(len(df)):
    series_1.append(df.iloc[i,coloumn_no_1])   
    series_2.append(df.iloc[i,coloumn_no_2])

X = np.asarray(map(lambda (x,y): [x, y], zip(series_1, series_2)))

clf = svm.OneClassSVM(nu=0.1, kernel="rbf", gamma=0.1)
clf.fit(X)

test_data = np.array([[3,2500],[4,5000],[5,600],[3,3000],[4,2500],[6,2500]])
pred_test = clf.predict(test_data)
print "Anomaly value: ", type(pred_test)

plt.scatter(X[:, 0], X[:, 1])
plt.scatter(test_data[:, 0], test_data[:, 1])
plt.show()


# In[63]:




# In[ ]:




# In[ ]:



