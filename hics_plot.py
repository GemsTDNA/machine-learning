# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
plt.style.use('ggplot')
from scipy.io import arff
import pandas as pd
from scipy import stats
import itertools
import random
import numpy as np
from sklearn.neighbors import NearestNeighbors
from sklearn.preprocessing import MinMaxScaler
from sklearn import metrics

#We use arff from scipy.io to read the file
#Download the data from here http://ipd.kit.edu/~muellere/HiCS/

data, meta = arff.loadarff('C:/Users/lingaselvan/Documents/Python Scripts/Data/ann_thyroid.arff')
#data, meta = arff.loadarff('C:/Users/lingaselvan/Documents/Python Scripts/Data/hics_data.arff')
df = pd.DataFrame(data)

plt.figure(0)
ax1 = plt.subplot2grid((3,3), (0,0), colspan=2)
ax2 = plt.subplot2grid((3,3), (1,0), colspan=2, rowspan=2,sharex=ax1)
ax3 = plt.subplot2grid((3,3), (1, 2), rowspan=2,sharey=ax2)


plt.suptitle("High Contrast Subspace: var_0002 & var_0003")

ax2.scatter(df['var_0003'],df['var_0002'])
df['var_0003'].hist(color='k', alpha=0.5, bins=30,ax=ax1,normed = True)
df['var_0002'].hist(color='k', alpha=0.5, bins=30,orientation="horizontal")

var0002_index = df['var_0002'].rank()/max(df['var_0002'].rank())
df[(var0002_index>0.65)]['var_0003'].hist(color='r', alpha=0.2, bins=20,ax=ax1,normed = True,linestyle='dotted')
ax1.text(0.8,5,'p-value:<0.0001***',va="center", ha="center")
plt.show()

#Below lines calculates Weltch's t-test and Kolmogorov-Smirnov statistics

t=stats.ttest_ind(df['var_0003'].values, df[(var0002_index>0.65)]['var_0003'].values, equal_var = False)
print "t-test"
print t.pvalue

k = stats.ks_2samp(df['var_0003'].values, df[(var0002_index>0.65)]['var_0003'].values)
print "k-test"
print k.pvalue

#Now we show a low contrast subspace
plt.figure(1)
ax1 = plt.subplot2grid((3,3), (0,0), colspan=2)
ax2 = plt.subplot2grid((3,3), (1,0), colspan=2, rowspan=2,sharex=ax1)
ax3 = plt.subplot2grid((3,3), (1, 2), rowspan=2,sharey=ax2)


plt.suptitle("Low Contrast Subspace: var_0000 & var_0003")

ax2.scatter(df['var_0000'],df['var_0003'])
df['var_0000'].hist(color='k', alpha=0.5, bins=30,ax=ax1,normed = True)
df['var_0003'].hist(color='k', alpha=0.5, bins=30,orientation="horizontal")

var0003_index = df['var_0003'].rank()/max(df['var_0003'].rank())
df[(var0003_index>0.65)]['var_0000'].hist(color='r', alpha=0.2, bins=30,ax=ax1,normed = True,linestyle='dotted')
ax1.text(0.8,5,'p-value:<0.35',va="center", ha="center")
plt.show()

t=stats.ttest_ind(df['var_0000'].values, df[(var0003_index>0.65)]['var_0000'].values, equal_var = False)
print "t-test"
print t.pvalue

k=stats.ks_2samp(df['var_0000'].values, df[(var0003_index>0.65)]['var_0000'].values)
print "k-test"
print k.pvalue