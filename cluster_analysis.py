# -*- coding: utf-8 -*-
"""
Created on Wed Sep 27 09:50:57 2017

@author: lingaselvan
"""

import random
import numpy as np
import pandas as pd
from matplotlib import style
style.use("ggplot")
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans

#x = [1, 2, 3, 7, 8, 9]
#y = [10, 20, 30, 70, 80, 90]
#x = random.sample(range(1, 200), 100)
#y = random.sample(range(200, 400), 100)

coloumn_no_1 = 0
coloumn_no_2 = 1

df = pd.read_excel('C:\Users\lingaselvan\Documents\Python Scripts\Data\cluster_data.xlsx')

series_1 = []
series_2 = []

for i in range(len(df)):
    series_1.append(df.iloc[i,coloumn_no_1])   
    series_2.append(df.iloc[i,coloumn_no_2])

print series_1[0]
print series_2[0]

x = series_1
y = series_2

plt.scatter(x,y)
plt.show()

X = np.array([[i[0], i[1]] for i in zip(x,y)])

print X

kmeans = KMeans(n_clusters=2)
kmeans.fit(X)

centroids = kmeans.cluster_centers_
labels = kmeans.labels_

print(centroids)
print(labels)

colors = ["g.","r.","c.","y.","b."]

for i in range(len(X)):
    print("coordinate:",X[i], "label:", labels[i])
    plt.plot(X[i][0], X[i][1], colors[labels[i]], markersize = 10)


plt.scatter(centroids[:, 0],centroids[:, 1], marker = "x", s=150, linewidths = 5, zorder = 10)

plt.show()