
# coding: utf-8

# In[25]:

import numpy as np
import pandas as pd
from sklearn import svm
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.metrics import accuracy_score
from sklearn import datasets, preprocessing
from sklearn.model_selection import train_test_split


# In[26]:

coloumn_no_1 = 0
coloumn_no_2 = 1
coloumn_no_3 = 2

df = pd.read_excel('C:\Users\lingaselvan\Documents\Python Scripts\Data\svm_classification.xlsx')

series_1 = []
series_2 = []
series_3 = []
for i in range(len(df)):
    series_1.append(df.iloc[i,coloumn_no_1])   
    series_2.append(df.iloc[i,coloumn_no_2])
    series_3.append(df.iloc[i,coloumn_no_3])   

print series_1[0]
print series_2[0]
print series_3[0]

metrics_data = np.asarray(map(lambda (x, y, z): [x, y, z], zip(series_1, series_2, series_3)))

target_value = []
for i in range(len(df)):
    if ((series_1[i] < 400) and (series_2[i] < 400) and (series_3[i] < 400)):
        target_value.append(1)
    else:
        target_value.append(0)


# In[31]:

pca = PCA(n_components=2)
reduced_data_pca = pca.fit_transform(metrics_data)


# In[38]:

X, y = reduced_data_pca, np.asarray(target_value)

X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=33)

print len(X_train)
print len(y_train)

scaler = preprocessing.StandardScaler().fit(X_train)
X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test)

svc_model = svm.SVC(gamma=0.1, C=100., kernel='linear')

svc_model.fit(X_train, y_train)

y_pred = svc_model.predict(X_test)

print svc_model.score(X_test, y_test)
print accuracy_score(y_test, y_pred)


# In[39]:

test_data =  np.asarray([[1, 2, 3],[500, 600, 700],[5, 300, 10]])
y_pred = svc_model.predict(scaler.transform(pca.fit_transform(test_data)))
#y_test =  np.asarray([[1],[0],[1]])


# In[40]:

#print test_data
#print pca.fit_transform(test_data)
print y_pred
#print accuracy_score(y_test, y_pred)


# In[ ]:




# In[ ]:



